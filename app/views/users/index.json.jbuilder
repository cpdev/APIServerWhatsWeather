json.array!(@users) do |user|
  json.extract! user, :id, :name, :email, :password_digest, :token, :idRedeSocial, :password_reset_key, :password_reset_sent_at
  json.url user_url(user, format: :json)
end
